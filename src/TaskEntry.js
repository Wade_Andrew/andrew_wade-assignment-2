import React, { useState } from 'react';
import './App.css';

const TaskEntry = ({ addTask }) => {

  const [taskInput, setTaskInput] = useState("");

  const handleChange = (e) => {
    setTaskInput(e.currentTarget.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    addTask(taskInput);
    setTaskInput("");
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input type="text"
          autoFocus
          value={taskInput}
          onChange={handleChange}
          placeholder="Enter task"
          required />
        <button type="submit">+</button>
      </form>
    </div>
  );
};

export default TaskEntry;