import React from 'react';
import './App.css';

const Header = () => {
  let time = new Date().getHours();
  let greeting = "";

  if (time < 12) {
    greeting = "Morning";
  }
  else if (time > 12 && time < 18) {
    greeting = "Afternoon";
  }
  else {
    greeting = "Evening";
  }

  return (
    <div className="header">
      <h1>Good {greeting}!</h1>
      <p>What's on the agenda today?</p>
    </div>
  )
}

export default Header;