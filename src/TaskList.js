import React from 'react';
import TaskItem from "./TaskItem";

const TaskList = ({ taskList, handleTaskToggle, taskDelete }) => {
  return (
    <div>
      <button onClick={taskDelete} className="clear-button">Clear completed tasks</button>
      <p>Click tasks to mark as complete</p>
      <hr />
      {taskList.map(taskItem => {
        return (
          <TaskItem taskItem={taskItem}
            handleTaskToggle={handleTaskToggle}
            taskDelete={taskDelete} />
        )
      })}
    </div>
  );
};

export default TaskList;