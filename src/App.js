import React, { useState } from "react";
import Header from "./Header";
import TaskList from "./TaskList";
import TaskEntry from "./TaskEntry";
import Footer from "./Footer";
import './App.css';

function App() {
  const [taskList, setTaskList] = useState([]);

  const handleTaskToggle = (id) => {
    let toggleTask = taskList.map(task => {
      return task.id == id ? { ...task, complete: !task.complete } : { ...task };
    });
    setTaskList(toggleTask);
  }

  const taskDelete = () => {
    let filteredTasks = taskList.filter(task => {
      return !task.complete;
    });
    setTaskList(filteredTasks);
  }

  const addTask = (taskInput) => {
    let taskListcopy = [...taskList];
    taskListcopy.push({ id: taskList.length + 1, task: taskInput, complete: false });
    setTaskList(taskListcopy);
  }

  return (
    <div className="App">
      <Header />

      <TaskEntry addTask={addTask} />

      <div className="list">
        <TaskList taskList={taskList}
          handleTaskToggle={handleTaskToggle}
          taskDelete={taskDelete} />
      </div>

      <Footer />
    </div>
  );
}

export default App;
