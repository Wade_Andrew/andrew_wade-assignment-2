import React from 'react';
import styled from "styled-components";
import { Fade } from "react-awesome-reveal";

const TaskItem = ({ taskItem, handleTaskToggle }) => {

    const handleClick = (e) => {
        e.preventDefault()
        handleTaskToggle(e.currentTarget.id)
    }

    const StyledUl = styled.ul`
        margin-bottom: 0.7rem;
        padding: 0.4rem;
        font-size: 1.2rem;
        border-radius: 5px;
        cursor: pointer;
        list-style: none;
        transition: all 0.3s ease-in-out;
            &:hover {
        background-color: rgb(85, 85, 85);
        color: white;
        }
        `;

    return (
        <Fade className="fade" duration={500} direction="down">
            <StyledUl>
                <li className={taskItem.complete ? "task-complete" : ""}
                    id={taskItem.id}
                    value={taskItem.task}
                    onClick={handleClick}>
                    {taskItem.task}
                </li>
            </StyledUl>
        </Fade>
    );
};

export default TaskItem;