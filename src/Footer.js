import React from 'react';
import './App.css';

let date = new Date();
let localTime = date.toLocaleString('en-US', {
  weekday: 'short',
  day: 'numeric',
  year: 'numeric',
  month: 'long'
});

const Footer = () => {
  return (
    <div className="footer">
      <p>{localTime}</p>
    </div>
  )
}

export default Footer;